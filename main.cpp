#include <QApplication>
#include <QTextEdit>

int main(int argv, char **args)
{
    QApplication app(argv, args);
    QTextEdit textEdit;
    textEdit.show();
    textEdit.setText("Hello");
    QColor colors[] = {QColor::fromRgb(128, 128, 0),QColor::fromRgb(132,0,234), QColor::fromRgb(50,155,234), QColor::fromRgb(255,0,122)};
    int numOfColors = sizeof(colors)/sizeof(QColor);
    int currentColor = 0;
    QObject::connect(&textEdit,&QTextEdit::cursorPositionChanged, [&](){currentColor = (currentColor+1)%numOfColors; textEdit.setTextColor(colors[currentColor]);});
    return app.exec();
}
